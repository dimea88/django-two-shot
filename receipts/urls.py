from django.urls import path
from receipts.views import (
    receipt_list,
    category_list,
    account_list,
)

# Register view in receipts app for path ""
urlpatterns = [
    path("", receipt_list, name="home"),
    path("accounts/", account_list, name="account=list"),
    path("categories/", category_list, name="category_list"),
]
